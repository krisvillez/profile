Title: 
Author: Kris Villez
Date: 2019-03-19 07:42
Category: drop
Tags: me
Slug: about
save_as: index.html
status: hidden

My name is Kris Villez. I currently work as at the Oak Ridge National Laboratory (ORNL) in Tennessee, USA. In my work, I focus on process monitoring, diagnostics, and automation in complex environments. To solve the associated challenges, I deploy and develop tools for machine learning, mechanistic model identification, and optimization, and combine these with common sense and varying amounts of domain expertise.

The information on these pages does not reflect any opinion held at ORNL or any project executed there.

Kris

&nbsp; 
 
&nbsp; 
 
[CV]({static}/pdf/KrisVILLEZ_CV_2025-01-28.pdf) (last update: 2025/01/28)

[Publication List]({static}/pdf/KrisVILLEZ_Publications_current.pdf) (last update: 2025/02/26)