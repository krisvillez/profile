#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Kris'
SITENAME = 'Kris Villez'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/New_York'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (   ('Blog', 'https://krisvillez.gitlab.io/data-tinker-drop'),
            ('GitLab', 'https://gitlab.com/krisvillez'),
            ('LinkedIn', 'https://www.linkedin.com/in/krisvillez'),
            ('ORNL', 'https://www.ornl.gov/staff-profile/kris-villez/'),)

         
# Social widget
SOCIAL = (   )

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

OUTPUT_PATH = 'public/'
THEME = 'pelican-bootstrap3'
PLUGIN_PATHS = ['plugins/', ]
PLUGINS = ['i18n_subsites', ]
JINJA_ENVIRONMENT = {
    'extensions': ['jinja2.ext.i18n'],
}

BOOTSTRAP_THEME = 'slate'
PYGMENTS_STYLE = 'solarizeddark'


ARTICLE_PATHS = ['article']
STATIC_PATHS = ['img', 'pdf']
PAGE_PATHS = ['page']
ARTICLE_URL = 'article/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'article/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'

PAGE_URL = 'page/{slug}/'
PAGE_SAVE_AS = 'page/{slug}/index.html'
CATEGORY_URL = 'category/{slug}'
CATEGORY_SAVE_AS = 'category/{slug}/index.html'
TAG_URL = 'tag/{slug}'
TAG_SAVE_AS = 'tag/{slug}/index.html'

# Content licensing: permit derivatives, permit commercial use
CC_LICENSE_DERIVATIVES = "yes"
CC_LICENSE_COMMERCIAL = "yes"